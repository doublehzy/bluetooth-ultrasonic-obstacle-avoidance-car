#include <reg51.h>
#include  <intrins.h>
#include  <STDIO.H>
#include  <stdio.h>
#include  <stdlib.h>

#define jingzhen     11059200UL             /*使用22.1184M晶体*/     //
#define botelv   9600UL             /*波特率定义为9600*/

#define uchar unsigned  char
#define uint  unsigned   int  
	
sbit LED_duan=P2^6;
sbit LED_wei=P2^7;

sbit led_0=P0^0;

char code led8_code[]={0x3F,0x06,0x5B,0x4f,0X66,0X6D,0X7D,0X07,0X7F,0X6F,0};
	
/*电机驱动IO定义*/
sbit IN1=P1^2; //为1 左电机反转
sbit IN2=P1^3; //为1 左电机正转
sbit IN3=P1^6; //为1 右电机正转
sbit IN4=P1^7; //为1 右电机反转
sbit EN1=P1^4; //为1 左电机使能
sbit EN2=P1^5; //为1 右电机使能
#define left_motor_en            EN1=1 //左电机使能
#define left_motor_stops        EN1=0 //左电机停止
#define right_motor_en          EN2=1 //右电机使能
#define right_motor_stops        EN2=0 //右电机停止

#define left_motor_go            IN1=0,IN2=1 //左电机正转
#define left_motor_back          IN1=1,IN2=0 //左电机反转
#define right_motor_go          IN3=1,IN4=0 //右电机正转
#define right_motor_back        IN3=0,IN4=1 //右电机反转
#define motor_relax        IN1=0,IN2=0,IN3=0,IN4=0 //惯性运动
	
sbit RX = P2 ^ 0;
sbit  TX=P2^1;
sbit s2 = P3 ^ 4;   //按钮1
sbit bee = P2 ^ 3;

unsigned int  time=0;
unsigned int  timer=0;
float         S=0;
bit           flag =0;
unsigned char zifu = 'N';            //待显示字符。
volatile unsigned char sending;
int k;

int serial_car_go_flag;
int car_go_flag;
float length;
int i;

void delay(unsigned char n) //@11.0592MHz
{
	while(n--){
	unsigned char i, j;
	_nop_();
	i = 2;
	j = 50;
	do
	{
		while (--j);
	} while (--i);
}  
}

void delayled(unsigned int i)
{
	for(;i>0;i--)
	{
		int j=110;
		for(;j>0;j--)
		{
			
		}
	}
}

// {  for(;i>0;i--){
//		int j=110;
//	 for(;j>0;j--){}
// }


void init(void)				//串口初始化
{
		/*******定时器和串口的初始化**********/
  TMOD=0x21;		   //设T0为方式1，GATE=1；
	SCON=0x50;
	TH1=0xFD;
	TL1=0xFD;
	TH0=0;
	TL0=0; 
	TR0=0;  
	ET0=1;             //允许T0中断
	TR1=1;			   //开启定时器
	TI=1;
	ES=1;         //串行中断允许
//  REN=1;        //允许接收 
	EA=1;			   //开启总中断
}

void send(unsigned char d)		  //发送一个字节的数据，形参d即为待发送数据。
{
 SBUF=d; //将数据写入到串口缓冲
	//如果不是中断函数内部发送数据，不能加下面两句
 sending=1;	 //设置发送标志
 while(sending); //等待发送完毕
}

void Send_Bit(char Data)//发送一个字符
{
    SBUF = Data;
    while(!TI);
    TI = 0;
}

void Send_String(char * p)//发送一个字符串
{
    while(*p != '\0')
    {
        Send_Bit(*p);
        p++;
    }
}

/********************************************************/
    float Conut(void)
	{
		char str[20]={0};
	 time=TH0*256+TL0;
	 TH0=0;
	 TL0=0;
	 S=(time*1.87)/100;     //算出来是CM
	 if(flag==1)		    //超出测量
	 {
	  flag=0;
		   
				Send_String("障碍物超过测量距离!!\n");
	 }
	 else
	 {
	    	 
					sprintf(str,"lenth=%.2f\n",S);
	        Send_String(str);
	 }
		return S/1;
	}
/********************************************************/ 

/********************************************************/
void zd0() interrupt 1 		 //T0中断用来计数器溢出,超过测距范围
  {
    flag=1;							 //中断溢出标志
  }
/********************************************************/
   void  StartModule() 		         //T1中断用来扫描数码管和计800MS启动模块
  {
	  TX=1;			                 //800MS  启动一次模块
	  _nop_(); //是汇编语言 的NOP指令  维持高电平时间超过10us
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_();
	  _nop_(); 
	  _nop_(); 
	  _nop_(); 
	  _nop_();
	  TX=0;
  }
/********************************************************/

float get_forward_length(){
			EA=0;
		  StartModule();	//就是在 Trig这个脚 触发超声波声音
			while(!RX);		//当RX为零时等待，只要 Rx ECHO脚为低电平，那么CPU就一直卡这里，直到等到变成高电平为止，才结束循环
			TR0=1;			  //开启计数，开始计数
			while(RX);		//当RX为1计数并等待
			TR0=0;
			Conut();
			EA=1;
			return S;
	}

void close_link(){
		TI=0;
		sending=0;  //清正在发送标志
}

void car_go(int flag) {
		left_motor_en;
		right_motor_en;
	
    switch (flag) {
        case 1: {//前进1
            left_motor_go;
            right_motor_go;
            delay(4);
            motor_relax;
            delay(6);
            break;
        }
        case 2: {//前进2
            left_motor_go;
            right_motor_go;
            delay(7);
            motor_relax;
            delay(3);
            break;
        }
        case 3: {//前进3
            left_motor_go;
            right_motor_go;
            delay(10);
            motor_relax;
            delay(0);
            break;
        }
        case 4: {//后退1
            left_motor_back;
            right_motor_back;
            delay(3);
            motor_relax;
            delay(7);
            break;
        }
        case 5: {//后退2
            left_motor_back;
            right_motor_back;
            delay(7);
            motor_relax;
            delay(3);
            break;
        }
        case 6: {//后退3
            left_motor_back;
            right_motor_back;
            delay(10);
            motor_relax;
            delay(0);
            break;
        }
        case 7: {//右转
            left_motor_go;
            right_motor_go;
            delay(3);
            motor_relax;
            left_motor_go;
            delay(4);
            motor_relax;
            delay(3);
            break;
        }
				case 8: {//左转
            left_motor_go;
            right_motor_go;
            delay(3);
            motor_relax;
            right_motor_go;
            delay(4);
            motor_relax;
            delay(3);
            break;
        }
				case 9: {//停车       
					  IN1=1,IN2=1,IN3=1,IN4=1;									
            delay(10);
            break;
        }
	
    }
    return;
}

void main() {
	  LED_wei=1;//不要让位被锁存
		P0=0XFF;//让8个P0脚全部输出高电平
		led_0=0;
		LED_wei=0;//锁住
		LED_duan=1;
	  P0=led8_code[3];
    init(); // 初始化
		serial_car_go_flag=0;
    S = 100;
    car_go_flag = 0;
    while (1) {
			if(!serial_car_go_flag){
        S = get_forward_length();
        if (S < 40.0 && S > 20.0) {
            //转弯
						delay(10);
            car_go_flag = 7;
						bee=0;
						delay(400);
						bee=1;
        } 
				else if (S < 20.0) 
				{//后退
            car_go_flag = 5;
        } 
				else 
				{//直行
						delay(10);
            car_go_flag = 3;
				}	 
        for (i = 30; i > 0; i--) 
				{
            car_go(car_go_flag);
        }
      }
			else
			{
					car_go(serial_car_go_flag);
			}    
  }
}

		
//串口接受函数，串口接受中断，一旦收到一个字节，CPU自动调用

char revdata;
void uart(void) interrupt 4		 //串口发送中断
{
	if(RI)    //收到数据
	{
  RI=0;   //清中断请求
	 revdata=SBUF;
	 if(revdata >= 48 && revdata <= 57 ){
		  delay(20);
			serial_car_go_flag=revdata-48;
			SBUF=revdata;
	}
	 else {
			SBUF=revdata;
		  delay(20);
	}
	 }
 else      //发送完一字节数据
 {
  close_link();
 }
}