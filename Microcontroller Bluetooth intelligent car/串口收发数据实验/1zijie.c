#include <reg51.h>

#define jingzhen     11059200UL             /*使用22.1184M晶体*/     //
#define botelv   9600UL             /*波特率定义为9600*/

/*电机驱动IO定义*/
sbit IN1=P1^2; //为1 左电机反转
sbit IN2=P1^3; //为1 左电机正转
sbit IN3=P1^6; //为1 右电机正转
sbit IN4=P1^7; //为1 右电机反转
sbit EN1=P1^4; //为1 左电机使能
sbit EN2=P1^5; //为1 右电机使能
#define left_motor_en            EN1=1 //左电机使能
#define left_motor_stops        EN1=0 //左电机停止
#define right_motor_en          EN2=1 //右电机使能
#define right_motor_stops        EN2=0 //右电机停止

#define left_motor_go            IN1=0,IN2=1 //左电机正转
#define left_motor_back          IN1=1,IN2=0 //左电机反转
#define right_motor_go          IN3=1,IN4=0 //右电机正转
#define right_motor_back        IN3=0,IN4=1 //右电机反转
#define motor_relax        IN1=0,IN2=0,IN3=0,IN4=0 //惯性运动

void delay(unsigned char i)
{
	unsigned char j,k;
	for(j=i;j>0;j--)
		for(k=90;k>0;k--);
}

int car_go_flag;
unsigned char zifu = 'N';            //待显示字符。
volatile unsigned char sending;
sbit s2 = P3 ^ 4;   //按钮1
sbit bee = P2 ^ 3;
sbit LED_duan = P2 ^ 6;
sbit LED_wei = P2 ^ 7;

sbit led_a = P0 ^ 0;
sbit led_b = P0 ^ 1;
sbit led_c = P0 ^ 2;
sbit led_d = P0 ^ 3;
sbit led_e = P0 ^ 4;
sbit led_f = P0 ^ 5;
sbit led_g = P0 ^ 6;
sbit led_dp = P0 ^ 7;
sbit led_0 = P0 ^ 0;
sbit led_1 = P0 ^ 1;
sbit led_2 = P0 ^ 2;
sbit led_3 = P0 ^ 3;
sbit led_4 = P0 ^ 4;
sbit led_5 = P0 ^ 5;
sbit led_6 = P0 ^ 6;
sbit led_7 = P0 ^ 7;

char code
led_code[]={
0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F,0x77,0x7C,0x39,0x5E,0x79,0x71,0x00};



void car_go(int flag) {
	if(flag==1){
		left_motor_go;
    right_motor_go;
    delay(5);
    motor_relax;
    delay(15);
	}
	if(flag==2){
		left_motor_go;
    right_motor_go;
    delay(10);
    motor_relax;
    delay(10);
	}
	if(flag==3){
		left_motor_go;
    right_motor_go;
    delay(15);
    motor_relax;
    delay(5);
	}
	if(flag==4){
		left_motor_back;
    right_motor_back;
    delay(5);
    motor_relax;
    delay(15);
	}
	if(flag==5){
		left_motor_back;
    right_motor_back;
    delay(10);
    motor_relax;
    delay(10);
	}
	if(flag==6){
		left_motor_back;
    right_motor_back;
    delay(15);
    motor_relax;
    delay(5);
	}
	if(flag=='r'){//右转
		left_motor_go;
    right_motor_go;
    delay(5);
    motor_relax;
    left_motor_go;
    delay(10);
    motor_relax;
    delay(5);
	}
	if(flag=='l'){//左转
		left_motor_go;
    right_motor_go;
    delay(5);
    motor_relax;
    right_motor_go;
    delay(10);
    motor_relax;
    delay(5);
	}
}

//void car_go(int flag) {
//    switch (flag) {
//        case 1: {//一档前进
//            left_motor_go;
//            right_motor_go;
//            delay(5);
//            motor_relax;
//            delay(15);
//            break;
//        }
//        case 2: {//二挡前进
//            left_motor_go;
//            right_motor_go;
//            delay(10);
//            motor_relax;
//            delay(10);
//            break;
//        }
//        case 3: {//三挡前进
//            left_motor_go;
//            right_motor_go;
//            delay(15);
//            motor_relax;
//            delay(5);
//            break;
//        }
//        case 4: {//一档后退
//            left_motor_back;
//            right_motor_back;
//            delay(5);
//            motor_relax;
//            delay(15);
//            break;
//        }
//        case 5: {//二挡后退
//            left_motor_back;
//            right_motor_back;
//            delay(10);
//            motor_relax;
//            delay(10);
//            break;
//        }
//        case 6: {//三挡后退
//            left_motor_back;
//            right_motor_back;
//            delay(15);
//            motor_relax;
//            delay(5);
//            break;
//        }

//        case 7: {//右转
//            left_motor_go;
//            right_motor_go;
//            delay(5);
//            motor_relax;
//            left_motor_go;
//            delay(10);
//            motor_relax;
//            delay(5);
//            break;
//        }
//				case 8: {//左转
//            left_motor_go;
//            right_motor_go;
//            delay(5);
//            motor_relax;
//            right_motor_go;
//            delay(10);
//            motor_relax;
//            delay(5);
//            break;
//        }
//    }
//    return;
//}

void led_light(int i) {
    LED_duan = 1;
    switch (i) {
        case 0: {
            P0 = led_code[0];
//            P0 = 0xFF;
//            led_g = 0;
//            led_dp = 0;
            break;
        }
        case 1: {
            P0 = led_code[1];
//            P0 = 0x00;
//            led_b = 1;
//            led_c = 1;
            break;
        }
        case 2: {
            P0 = led_code[2];
//            P0 = 0xff;
//            led_f = 0;
//            led_c = 0;
//            led_dp = 0;
            break;
        }
        case 3: {
            P0 = led_code[3];
//            P0 = 0xff;
//            led_f = 0;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 4: {
            P0 = led_code[4];
//            P0 = 0xff;
//            led_a = 0;
//            led_d = 0;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 5: {
            P0 = led_code[5];
//            P0 = 0xff;
//            led_b = 0;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 6: {
            P0 = led_code[6];
//            P0 = 0xff;
//            led_b = 0;
//            led_dp = 0;
            break;
        }
        case 7: {
            P0 = led_code[7];
//            P0 = 0x00;
//            led_a = 1;
//            led_b = 1;
//            led_c = 1;
            break;
        }
        case 8: {
            P0 = led_code[8];
//            P0 = 0xff;
//            led_dp = 0;
            break;
        }
        case 9: {
            P0 = led_code[9];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 10: {
            P0 = led_code[10];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 11: {
            P0 = led_code[11];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 12: {
            P0 = led_code[12];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 13: {
            P0 = led_code[13];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 14: {
            P0 = led_code[14];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
        case 15: {
            P0 = led_code[15];
//            P0 = 0xff;
//            led_e = 0;
//            led_dp = 0;
            break;
        }
    }
    LED_duan = 0;
    return;
}

void led_wei(int i) 
	{
    LED_wei = 1;// 不让位锁存
    P0 = 0xFF;
    switch (i) {
        case 0:
            led_0 = 0;
            break;
        case 1:
            led_1 = 0;
            break;
        case 2:
            led_2 = 0;
        case 3:
            led_3 = 0;
        case 4:
            led_4 = 0;
        case 5:
            led_5 = 0;
        case 6:
            led_6 = 0;
        case 7:
            led_7 = 0;
    }
    LED_wei = 0;//锁存
    return;
}

void init(void)				//串口初始化
{
 EA=0; //暂时关闭中断
 TMOD&=0x0F;  //定时器1模式控制在高4位
 TMOD|=0x20;    //定时器1工作在模式2，自动重装模式
 SCON=0x50;     //串口工作在模式1
 TH1=256-jingzhen/(botelv*12*16);  //计算定时器重装值
 TL1=256-jingzhen/(botelv*12*16);
 PCON|=0x80;    //串口波特率加倍
 ES=1;         //串行中断允许
 TR1=1;        //启动定时器1
 REN=1;        //允许接收 
 EA=1;         //允许中断
}

void send(unsigned char d)		  //发送一个字节的数据，形参d即为待发送数据。
{
 
 SBUF=d; //将数据写入到串口缓冲
	
	//如果不是中断函数内部发送数据，不能加下面两句
 sending=1;	 //设置发送标志
 while(sending); //等待发送完毕
}

void main()
{
	init(); // 初始化
	while(1)
	{
		car_go(car_go_flag);
		if(s2==0)
		{
			delay(20);
			if(!s2)
			{
				while(!s2);		   
				send('N');
			}
		}
	}
}
//串口接受函数，串口接受中断，一旦收到一个字节，CPU自动调用
char revdata;
void uart(void) interrupt 4		 //串口发送中断
{
	if(RI)    //收到数据
	{
  RI=0;   //清中断请求
	 revdata=SBUF;
	 if(revdata >= 48&& revdata <= 57 ){
		 	led_wei(0);
			led_light(revdata-48);
			SBUF=revdata;
		  delay(20);
			car_go_flag=revdata-48;
	}
	 if(revdata >= 97&& revdata <= 123 || revdata >= 65&& revdata <= 91 ){
			car_go_flag=revdata;
	 }
	 
//	 if(revdata >= 97&& revdata <= 123 ){
//		  SBUF=revdata-32;
//		   bee=0;
//			delay(10);
//			bee=1;
//	 }
//	 else if(revdata >= 97&& revdata <= 123 || revdata >= 65&& revdata <= 91 ){
//			SBUF=revdata;
//		  bee=0;
//			delay(10);
//			bee=1;
//	 }
//	 else if(revdata >= 48&& revdata <= 57){
//			led_wei(0);
//			led_light(revdata-48);
//		  delay(20);
//	 }
	 else {
			led_wei(0);
			led_light(12);
			SBUF=revdata;
		  delay(20);
	}
	 }
 else      //发送完一字节数据
 {
	
  TI=0;
  sending=0;  //清正在发送标志
 }
}