#include "reg51.h"

//每一个数码管有8段，a b c d e f g dp
sbit LED_duan=P2^6;
sbit LED_wei=P2^7;

sbit led_a=P0^0;
sbit led_b=P0^1;
sbit led_c=P0^2;
sbit led_d=P0^3;
sbit led_e=P0^4;
sbit led_f=P0^5;
sbit led_g=P0^6;
sbit led_dp=P0^7;

sbit led_0=P0^0;
sbit led_1=P0^1;
sbit led_2=P0^2;
sbit led_3=P0^3;
sbit led_4=P0^4;
sbit led_5=P0^5;
sbit led_6=P0^6;
sbit led_7=P0^7;

char code led8_code[]={0x3F,0x06,0x5B,0x4f,0X66,0X6D,0X7D,0X07,0X7F,0X6F,0};

void delayled(unsigned int i)
{
	for(;i>0;i--)
	{
		int j=110;
		for(;j>0;j--)
		{
			
		}
	}
}
char var=13;
int j=1;
//低电平锁存
void main()
{	
	for(;var>0;var--){
		for(j=1;j<50;j++){
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[var/10];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[var%10];
			LED_duan=0;
			delay(10);
		}
	}
	//显示12
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[1];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[2];
			LED_duan=0;
			delay(10);
	//显示11
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[1];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[1];
			LED_duan=0;
			delay(10);
	//显示10
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[1];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
	//显示9
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[9];
			LED_duan=0;
			delay(10);
	//显示8
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[8];
			LED_duan=0;
			delay(10);
	//显示7
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[7];
			LED_duan=0;
			delay(10);
 //显示6
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[6];
			LED_duan=0;
			delay(10);
//显示5
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[5];
			LED_duan=0;
			delay(10);
//显示4
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[4];
			LED_duan=0;
			delay(10);	
//显示3
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[3];
			LED_duan=0;
			delay(10);		
//显示2
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[2];
			LED_duan=0;
			delay(10);		
//显示1
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[1];
			LED_duan=0;
			delay(10);
//显示0
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_0=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);
		
			LED_wei=1;//不要让位被锁存
			P0=0XFF;//让8个P0脚全部输出高电平
			led_1=0;
			LED_wei=0;//锁住
			LED_duan=1;
			P0=led8_code[0];
			LED_duan=0;
			delay(10);

	



//	LED_wei=1;//不要让位被锁存
//	P0=0XFF;//让8个P0脚全部输出高电平
//	led_0=0;
//	LED_wei=0;//锁住
//	for(;;){
//	//0
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=1;
//	led_e=1;
//	led_f=1;
//	led_g=0;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//1
//	LED_duan=1;
//	led_a=0;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=0;
//	led_e=0;
//	led_f=0;
//	led_g=0;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
////2
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=0;
//	led_d=1;
//	led_e=1;
//	led_f=0;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//3
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=1;
//	led_e=0;
//	led_f=0;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//4
//	LED_duan=1;
//	led_a=0;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=0;
//	led_e=0;
//	led_f=1;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//5
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=0;
//	led_c=1;
//	led_d=1;
//	led_e=0;
//	led_f=1;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//6
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=0;
//	led_c=1;
//	led_d=1;
//	led_e=1;
//	led_f=1;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//7
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=0;
//	led_e=0;
//	led_f=0;
//	led_g=0;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//8
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=1;
//	led_e=1;
//	led_f=1;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//	//9
//	LED_duan=1;
//	led_a=1;//不亮
//	led_b=1;
//	led_c=1;
//	led_d=1;
//	led_e=0;
//	led_f=1;
//	led_g=1;
//	led_dp=0;
//	LED_duan=0;
//	delay(1000);
//}
	
	
}
